**Magento2+Nginx+PHP-FPM+MySQL+Redis+Varnish+Let's encrypt Deployment**

- Requires Ansible 2.6 or newer
- Expects Debian 9.x hosts

These playbooks deploy a simple all-in-one configuration of the popular
Magento2 eCommerce platform, frontend by the Nginx web server and the
PHP-FPM process manager. To use, copy the `hosts.example` file to `hosts` and 
edit the `hosts` inventory file to include the names or URLs of the servers
you want to deploy.

Then run the playbook, like this:

	ansible-playbook -i hosts site.yml

The playbooks will configure Magento2, Nginx, PHP-FPM, MySQL, Redis, Varnish, Let's encrypt. When the run
is complete, you can hit access server to begin the Magento2 eCommerce platform configuration.

### Ideas for Improvement

Here are some ideas for ways that these playbooks could be extended:

We would love to see contributions and improvements, so please fork this
repository on GitHub and send us your changes via pull requests.