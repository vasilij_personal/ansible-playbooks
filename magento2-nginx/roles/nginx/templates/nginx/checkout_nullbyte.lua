function write_log_file(content)
  local file = "/tmp/.nullbyte.log"
  local f = io.open(file, "a+")
  f:write(content .. "\n")
  f:close()
end

function block_nullbyte()
  ngx.exit(400)
end

function handle_nullbyte(body_data)
  write_log_file(body_data)
  block_nullbyte()
end

function when_nullbyte(body_data, proc)
  if string.find(body_data, '%%00') then
    proc(body_data)
  end
end

local checkout = {
  "/checkout/onepage/savebilling",
  "/checkout/onepage/savepayment",
  "/checkout/json/savebilling",
  "/checkout/json/savepayment",
}

for _,checkout_uri in pairs(checkout) do
  if string.match(string.lower(ngx.var.uri), checkout_uri) then
    ngx.req.read_body()
    --- Note: if the data does not fit in the client_body_buffer_size
    --- nginx writes the data to a file. We do not inspect this because
    --- it would greatly decrease performance. I think it's OK not to check
    --- if a large request like that was done because these pages generally
    --- don't send that much data but in case it turns out that we need to
    --- check anyway in the future we can use get_body_file
    --- https://github.com/openresty/lua-nginx-module#ngxreqget_body_file
    local body_data = ngx.req.get_body_data() or ""
    when_nullbyte(body_data, handle_nullbyte)
    break
  end
end


--- Do not tell nginx whether this handler found this request OK or not
return ngx.exit(ngx.DECLINED)
