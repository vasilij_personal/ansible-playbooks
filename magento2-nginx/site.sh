#!/bin/bash

password=`date +%s | sha256sum | base64 | head -c 32`
sed -i 's/secret/'$password'/g' group_vars/all

password=`date +%s | sha256sum | base64 | head -c 32`
sed -i 's/secret2/'$password'/g' group_vars/all
sed -i 's/secret2/'$password'/g' roles/magento2/defaults/main.yml

ansible-playbook -i hosts site.yml